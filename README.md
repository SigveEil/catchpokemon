--API--
This application uses the pokeAPI to retrieve information about pokemon, all the requests are 95% done

--Login--
Uses JSON database to store user information as well as the collected pokemon

--Local Storage--
This application stores the user ID in the localStorage to make sure they are logged in to access the site

--Styling--
This application uses Bootstrap styling

--bugs and errors--
This application features unique bugs and errors when interacting with the pokeAPI, everything that looks like it works, doesn't.
