import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PokemonService {
  private _pokemons: string[] = [];
  private _error: string = '';

  constructor(private readonly http: HttpClient) {}

  //Fetches the original 152 pokemon
  //Attempts to add them to an array to access them later
  public fetchGoodPokemon(): void {
    this.http.get('https://pokeapi.co/api/v2/pokemon?limit=152').subscribe(
      (data) => {
        for (let i = 0; i < Response.length; i++) {
          this._pokemons.push(Response.name);
          console.log(this._pokemons);
        }
      },
      (error: HttpErrorResponse) => {
        this._error = error.message;
      }
    );
  }

  public pokemons() {
    return this._pokemons;
  }
}
