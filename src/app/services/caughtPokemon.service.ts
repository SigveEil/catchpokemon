import { Injectable } from '@angular/core';
import { CaughtPokemon } from '../models/user.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class CaughtPokemonService {
  private _pokemonName: string[] = [];
  private _pokemonUrl: string[] = [];
  private _error: string = '';
  //private user = localStorage.getItem('trainerID');

  constructor(private readonly http: HttpClient) {}

  //Getting the caught pokemon from the user
  public showPokemon(): void {
    this.http
      .get<CaughtPokemon>(
        `http://localhost:3000/users/${localStorage.getItem('trainerID')}`
      )
      .subscribe(
        (data) => {
          this._pokemonName.push(data.name);
          console.log(this._pokemonName);
          this._pokemonUrl.push(data.url);
        },
        (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      );
  }
  public pokemonName(): string[] {
    return this._pokemonName;
  }
  public pokemonUrl(): string[] {
    return this._pokemonUrl;
  }
  public error(): string {
    return this._error;
  }
}
