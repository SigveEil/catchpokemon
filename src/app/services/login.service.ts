import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  private _user: string = '';
  private _error: string = '';

  constructor(private readonly http: HttpClient) {}

  //Adding the user to the json database if they are not allready added.
  public loginOrAdd(id: User): void {
    this.http
      .post<User>('http://localhost:3000/users', {
        id,
        //Giving each user the best starter pokemon
        caughtPokemon: [
          {
            name: 'Squirtle',
            url: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/7.png',
          },
        ],
      })
      .subscribe(
        (data) => {
          this._user = data.id;
        },
        (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      );
  }
}
