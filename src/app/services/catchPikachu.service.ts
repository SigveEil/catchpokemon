import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CaughtPokemon } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class CatchPikachu {
  private _error: string = '';
  private user = localStorage.getItem('trainerID');
  constructor(private readonly http: HttpClient) {}

  //....This should work, it really should...
  public pokeballGo(): void {
    this.http
      .post<CaughtPokemon>(`http://localhost:3000/users/${this.user}`, {
        //Giving the current user a pikachu
        name: 'Pikachu',
        url: 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/25.png',
      })
      .subscribe((data) => {
        (error: HttpErrorResponse) => {
          this._error = error.message;
        };
      });
  }
}
