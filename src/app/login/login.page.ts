import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.css'],
})
//Implements OnInint because I read that it was good practice
export class loginPage implements OnInit {
  ngOnInit(): void {}

  constructor(
    private readonly loginService: LoginService,
    //Redirects user if they are allready logged in
    private router: Router
  ) {
    if (localStorage.getItem('trainerID') !== null) {
      router.navigate(['/catalogue']);
    }
  }

  //Logs in user and redirects them
  public onSubmit(createForm: NgForm): void {
    localStorage.setItem('trainerID', createForm.value.username);
    this.loginService.loginOrAdd(createForm.value.username);
    this.router.navigate(['/catalogue']);
  }
}
