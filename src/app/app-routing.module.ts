import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { cataloguePage } from './catalogue/catalogue.page';
import { loginPage } from './login/login.page';
import { trainerPage } from './trainer/trainer.page';

//Creating rooting between pages
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login',
  },
  {
    path: 'login',
    component: loginPage,
  },
  {
    path: 'catalogue',
    component: cataloguePage,
  },
  {
    path: 'trainer',
    component: trainerPage,
  },
  {
    path: '**',
    redirectTo: 'login',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
