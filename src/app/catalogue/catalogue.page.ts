import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CatchPikachu } from '../services/catchPikachu.service';
import { PokemonService } from '../services/pokemon.service';

@Component({
  selector: 'app-catalogue',
  templateUrl: 'catalogue.page.html',
  styleUrls: ['catalogue.page.css'],
})
//Implements OnInint because I read that it was good practice
export class cataloguePage implements OnInit {
  //Redirects user if they are not logged in
  constructor(
    private readonly pokemonService: PokemonService,
    private readonly catchPikachu: CatchPikachu,
    private router: Router
  ) {
    if (localStorage.getItem('trainerID') === null) {
      router.navigate(['/login']);
    }
  }

  ngOnInit(): void {
    this.pokemonService.fetchGoodPokemon;
  }

  get pokemons(): string[] {
    return this.pokemonService.pokemons();
  }

  public training() {
    this.router.navigate(['/trainer']);
  }

  public turnCap() {
    this.catchPikachu.pokeballGo();
  }
}
