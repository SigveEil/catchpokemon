import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CaughtPokemonService } from '../services/caughtPokemon.service';

@Component({
  selector: 'app-trainer',
  templateUrl: 'trainer.page.html',
  styleUrls: ['trainer.page.css'],
})
//Implements OnInint because I read that it was good practice
export class trainerPage implements OnInit {
  //Redirects user if they are not logged in
  constructor(
    private readonly caughtPokemonService: CaughtPokemonService,
    private router: Router
  ) {
    if (localStorage.getItem('trainerID') === null) {
      router.navigate(['/login']);
    }
  }
  ngOnInit(): void {
    this.caughtPokemonService.showPokemon();
  }

  get pokemonName(): string[] {
    return this.caughtPokemonService.pokemonName();
  }

  get pokemonUrl(): string[] {
    return this.caughtPokemonService.pokemonUrl();
  }

  //Goes to caughPokemon.service to try and find the users pokemon and display them in the console
  public clickPokemon() {
    console.log(this.pokemonName);
    console.log(this.pokemonUrl);
    console.log(
      'This is where your pokemon would appear, but there is currently something wrong with the fetch-request'
    );
  }

  //Returns user to catalogue
  public return() {
    this.router.navigate(['/catalogue']);
  }
}
