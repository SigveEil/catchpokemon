export interface User {
  id: string;
  caughtPokemon: CaughtPokemon;
}

export interface CaughtPokemon {
  name: string;
  url: string;
}
